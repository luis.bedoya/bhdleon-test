import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContainersRoutingModule } from './containers-routing.module';
import { LoginContainerComponent } from './login-container/login-container.component';
import { IonicModule } from '@ionic/angular';
import { ComponentsModule } from '../components/components.module';
import { ProductsContainerComponent } from './products-container/products-container.component';


@NgModule({
  declarations: [
    LoginContainerComponent,
    ProductsContainerComponent
  ],
  imports: [
    CommonModule,
    ContainersRoutingModule,
    IonicModule,
    ComponentsModule
  ], exports: [
    LoginContainerComponent,
    ProductsContainerComponent
  ]
})
export class ContainersModule { }
