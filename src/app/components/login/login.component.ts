/* eslint-disable @typescript-eslint/dot-notation */
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Storage } from '@ionic/storage-angular';
import { IUser } from 'src/app/core/interfaces/IUser';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  remindMe: boolean;
  formLogin: FormControl | any;
  user: IUser | undefined;
  // eslint-disable-next-line @typescript-eslint/member-ordering
  @Output() addLogin: EventEmitter<IUser> = new EventEmitter<IUser>();
  // eslint-disable-next-line @typescript-eslint/member-ordering
  @Output() checkRemindMe: EventEmitter<boolean> = new EventEmitter<boolean>();
  viewStatus: boolean;

  constructor(private formBuilder: FormBuilder, private storage: Storage) {
    this.remindMe = false;
    this.viewStatus = false;
    this.formLogin = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]]
    })
  }

  ngOnInit() {
    this.initStorage();
    this.getRemindMe();
  }
  async initStorage(){
    this.storage.create();
  }

  async getRemindMe(){
    const userRemindMe = await this.storage.get('user');
    if (userRemindMe) {
      // eslint-disable-next-line @typescript-eslint/dot-notation
      // eslint-disable-next-line @typescript-eslint/quotes
      this.formLogin.controls["username"].setValue(userRemindMe);
      this.remindMe = true;
    }
  }


  setValue(toogle: object | any){
    const { checked } = toogle;
    this.remindMe = checked;
  }

  public login(dataUser: any): void{
    if (this.formLogin.invalid) {
      this.viewStatus = true;
    } else {
      this.viewStatus = false;
    }

    this.user = {
      username: dataUser.username,
      password: dataUser.password
    };
    this.addLogin.emit(this.user);
    this.checkRemindMe.emit(this.remindMe);
  }

}
