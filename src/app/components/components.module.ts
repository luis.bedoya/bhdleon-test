import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ComponentsRoutingModule } from './components-routing.module';
import { LoginComponent } from './login/login.component';
import { MaterialModule } from '../utils/material/material.module';
import { ReactiveFormsModule, FormsModule} from '@angular/forms';

import { TranslateModule } from '@ngx-translate/core';
import { CardsComponent } from './cards/cards.component';
import { TransactionsListComponent } from './transactions-list/transactions-list.component';
import { IonicModule } from '@ionic/angular';


@NgModule({
  declarations: [
    LoginComponent,
    CardsComponent,
    TransactionsListComponent
  ],
  imports: [
    CommonModule,
    ComponentsRoutingModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
    TranslateModule,
    IonicModule
  ],exports:[
    LoginComponent,
    CardsComponent,
    TransactionsListComponent
  ]
})
export class ComponentsModule { }
