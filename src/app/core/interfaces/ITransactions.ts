export interface ITransactions{
  productNumber:   string;
  amount:          number;
  description:     string;
  transactionType: number;
  date:            Date;
}

