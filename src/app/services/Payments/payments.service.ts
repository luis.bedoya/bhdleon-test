import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { catchError, retry, map } from 'rxjs/operators';
import { Ipay } from 'src/app/core/interfaces/IPay';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PaymentsService {

  urlPay: string = `${environment.host}`;

  constructor(private http: HttpClient) { }

  public pay(body: Ipay): Observable<Ipay>{
    return this.http.post<Ipay>(this.urlPay + '/cards/payment', body)
    .pipe(catchError(err => of(err)));
  }



}
