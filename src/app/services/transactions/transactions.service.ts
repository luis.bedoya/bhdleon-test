import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { catchError, retry, map } from 'rxjs/operators';
import { ITransactions } from 'src/app/core/interfaces/ITransactions';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TransactionsService {

  urlTransactions: string = `${environment.host}`

  constructor(private http: HttpClient) { }

  public getTransactionsForProduct(numberProduct: string): Observable<ITransactions>{
    return this.http.get<ITransactions>(this.urlTransactions + '/cards/movements/' + numberProduct)
    .pipe(retry(1), catchError(this.CaptureError))
  }

  private CaptureError(error: HttpErrorResponse){
    return throwError(error);
  }

}
