import { Injectable, OnInit } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { Storage } from '@ionic/storage-angular';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate, OnInit {

  private _storage: Storage;
  tokenExists: boolean;

  constructor(private storage: Storage, private router: Router){}

  ngOnInit(): void {
    this.initStorage();
  }

  async initStorage(){
    this.storage.create();
  }

  canActivate():  boolean {
    try {
      this.storage.get('token').then((data)=> {
        if (data.length > 2 || data !== null) {
          this.tokenExists = true;
          this.router.navigateByUrl('/tabs');
          return true;
        } else {
          this.tokenExists = false;
          return false;
        }
      });
      return this.tokenExists;

    } catch (error) {
      console.log(error);
    }
  }

}
