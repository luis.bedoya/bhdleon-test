import { EventEmitter, Injectable, OnInit, Output } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { catchError, map, retry } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { Storage } from '@ionic/storage-angular';


@Injectable({
  providedIn: 'root'
})
export class AuthService implements OnInit {

  url: string = `${environment.host}`;
  private _storage: Storage;

  constructor(private http: HttpClient, private storage: Storage) { }

  ngOnInit(): void {
    this.initStorage();
  }

  async initStorage(){
    this.storage.create();
  }

  public login(username: string, password: string): Observable<any>{
    const body = {username, password};
    const headers = new HttpHeaders({'Content-Type': 'application/json'})
    return this.http.post<any>(this.url + '/login', body, { headers })
    .pipe(retry(4), catchError(err => of(err)));
  }

  public logOut(){
    this.storage.remove('token');
  }

  public getToken(): Observable<string>{
    return this.http.get<string>('../../../assets/json/token.json');
  }


}


