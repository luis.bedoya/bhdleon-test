import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ICards } from 'src/app/core/interfaces/ICards';
import { CardsService } from 'src/app/services/cards/cards.service';
import { ActionSheetController } from '@ionic/angular';
import { PaymentsService } from 'src/app/services/Payments/payments.service';
import { Ipay } from '../../core/interfaces/IPay';
import * as CryptoJS from 'crypto-js';
import { AuthService } from 'src/app/services/auth/auth.service';


@Component({
  selector: 'app-detail-product',
  templateUrl: './detail-product.page.html',
  styleUrls: ['./detail-product.page.scss'],
})
export class DetailProductPage implements OnInit {

  cardInfo: ICards;
  productNumber: string;
  public cards: ICards[] = [];
  nameClient: string;
  dateCard: string;
  RDActive: boolean;
  USActive: boolean;
  BalanceRD: number;
  BalanceUS: number;
  LimitRD: number;
  LimitUSS: number;
  progress: number;
  viewSpinner: boolean = false;
  secretKey: string;

  paymentOption: string = 'RD';
  constructor(private routeActive: ActivatedRoute,
              private cardService: CardsService,
              public actionSheetController: ActionSheetController,
              private payService: PaymentsService,
              private auth: AuthService) {
                this.RDActive = true;
              }

  ngOnInit() {
    this.getInfoUrl();
  }

  getInfoUrl(){
    this.productNumber = this.routeActive.snapshot.params.id;
    this.decryptParams(this.productNumber);
    this.getInfoCards();
  }

  decryptParams(param: string){
    this.getToken(param);
  }

  private getToken(param: string){
    let subscripction = this.auth.getToken().subscribe((data) => {
      let infoToken = Object.values(data);
      this.secretKey = infoToken[0].toString();
    }, err => {
      console.error(err);
    });
    subscripction.add(()=> {
      let id = CryptoJS.AES.decrypt(param.trim(), this.secretKey.trim()).toString(CryptoJS.enc.Utf8);
      this.productNumber = id;
    })
  }

  getInfoCards(){
    const subscripction = this.cardService.getCards().subscribe((data)=> {
      this.cards = Object.values(data)
    }, err => {
      console.error(err);
    });
    subscripction.add(() => {
      this.cardInfo = this.cards.filter(i => i.productNumber === this.productNumber)[0];
      let { clientName,
          productEndingDate,
          BalanceRD,
          BalanceUS,
          LimitRD,
          LimitUS } = this.cardInfo;
      this.nameClient = clientName;
      this.dateCard = productEndingDate;
      this.BalanceRD = BalanceRD;
      this.BalanceUS = BalanceUS;
      this.LimitRD = LimitRD;
      this.LimitUSS = LimitUS;
      this.progress =  (( this.BalanceRD /this.LimitRD ) * 100)/100;
    })
  }

  segmentChanged(event: any){
      this.paymentOption = event.detail.value;
      if (this.paymentOption === 'RD') {
        this.RDActive = true;
        this.USActive = false;
        this.progress =  (( this.BalanceRD /this.LimitRD ) * 100)/100;
      }else {
        this.USActive = true;
        this.RDActive = false;
        this.progress = ((this.BalanceUS / this.LimitUSS) * 100)/100;
      }
  }

  async pay() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Pago con exito',
      subHeader: 'El pago de tu tarjeta de credito se a realizado con exito',
      backdropDismiss: false,
      cssClass: 'detailsProducts__action-sheet',
      mode: 'md',

      buttons: [ {
        text: 'Cerrar',
        role: 'cancel',
        cssClass: 'btnActionSheet',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
}

makePayments(){
  this.viewSpinner = true;
  let bodyRequest: Ipay;
  if (this.paymentOption === 'RD') {
    bodyRequest = {
      amount: this.BalanceRD,
      productNumber: this.productNumber
    }
  } else {
    bodyRequest = {
      amount: this.BalanceUS,
      productNumber: this.productNumber
    }
  }
  this.payService.pay(bodyRequest).subscribe((data)=> {
    this.pay();
    setTimeout(() => {
      this.viewSpinner = false;
    }, 2000);
  },err => {
    console.error(err)
  })
}




}
