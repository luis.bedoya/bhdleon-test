import { Component, Input, OnInit } from '@angular/core';
import { ITransactions } from '../../core/interfaces/ITransactions';

@Component({
  selector: 'app-transactions-list',
  templateUrl: './transactions-list.component.html',
  styleUrls: ['./transactions-list.component.scss'],
})
export class TransactionsListComponent implements OnInit {

  @Input() infoTransactions: ITransactions[];

  constructor() { }

  ngOnInit() {}

  

}
