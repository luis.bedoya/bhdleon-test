export interface ICards {
  productNumber:     string;
  productBrand:      string;
  LimitRD:           number;
  BalanceRD:         number;
  LimitUS:           number;
  BalanceUS:         number;
  clientName:        string;
  productEndingDate: string;
}
