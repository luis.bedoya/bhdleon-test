import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController, MenuController, ToastController } from '@ionic/angular';
import { AuthService } from '../../services/auth/auth.service';
import { Storage } from '@ionic/storage-angular';
import { IUser } from 'src/app/core/interfaces/IUser';

@Component({
  selector: 'app-login-container',
  templateUrl: './login-container.component.html',
  styleUrls: ['./login-container.component.scss'],
})
export class LoginContainerComponent implements OnInit {
  remindMe: boolean;
  private _storage: Storage;
  errorMessage: string;
  @Output() tokenInterceptor: EventEmitter<string> = new EventEmitter<string>();

  constructor(private auth: AuthService,
              private router: Router,
              public loadingController: LoadingController,
              private storage: Storage,
              private menu: MenuController,
              public toastController: ToastController) { }

  ngOnInit() {
    this.initStorage();
  }

  async initStorage(){
    this.storage.create();
  }

  async presentToast() {
    const toast = await this.toastController.create({
      message: this.errorMessage,
      duration: 2000
    });
    toast.present();
  }

  CheckRemindMe(checked: boolean){
    this.remindMe = checked;
  }
  async presentLoading() {
    const loading = await this.loadingController.create({
      message: '',
      duration: 1000
    });
    await loading.present();
    const { role, data } = await loading.onDidDismiss();
  }

  addLogin(dataUser: IUser){
    let { username, password } = dataUser;
    this.auth.login(username, password).subscribe((response)=> {
      if (response.status === 404 || response.status === 400 || response.status === 0) {
        this.errorMessage = 'Error de conexion';
        this.presentToast();
      }else{
        if (this.remindMe) {
          this.storage.set('user', username);
        }else {
          this.storage.remove('user');
        }
        this.presentLoading();
        let { token } = response;
        this.storage.set('token', token);
        setTimeout(() => {
          //this.tokenInterceptor.emit(token);
          this.router.navigateByUrl('/tabs');
        }, 2000);
      }
    },(err)=> {
      console.error(err)
    })
  }

}
