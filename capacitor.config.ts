import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'bhdTest',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
