import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { EventEmitter, Injectable, OnInit, Output } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { catchError, retry, map, delay } from 'rxjs/operators';
import { ICards } from 'src/app/core/interfaces/ICards';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CardsService implements OnInit {

  urlCards: string = `${environment.host}`;
  @Output() InfoCard$: EventEmitter<ICards> = new EventEmitter<ICards>();

  constructor(private http: HttpClient) { }


  ngOnInit(): void {
    throw new Error('Method not implemented.');
  }

  public getCards(): Observable<ICards>{
    return this.http.get<ICards>(this.urlCards + '/cards')
    .pipe(delay(2000), retry(5), catchError(this.CaptureError))
  }

  private CaptureError(error: HttpErrorResponse){
    return throwError(error);
  }






}
