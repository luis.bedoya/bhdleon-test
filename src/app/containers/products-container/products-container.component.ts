import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ICards } from 'src/app/core/interfaces/ICards';
import { ITransactions } from 'src/app/core/interfaces/ITransactions';
import { CardsService } from 'src/app/services/cards/cards.service';
import { TransactionsService } from '../../services/transactions/transactions.service';
import { Router } from '@angular/router';
import * as CryptoJS from 'crypto-js';
import { AuthService } from '../../services/auth/auth.service';


@Component({
  selector: 'app-products-container',
  templateUrl: './products-container.component.html',
  styleUrls: ['./products-container.component.scss'],
})
export class ProductsContainerComponent implements OnInit {

  public cards: ICards[] = null;
  numberProduct: string;
  infoTransactions: ITransactions[] = null;
  firstInfo: ICards;
  @Output() InfoCard: EventEmitter<ICards> = new EventEmitter<ICards>();
  encryptedText: string;
  secretKey: string;

  slideOpts = {
    initialSlide: 0,
    speed: 400
  };

  constructor(private serviceCards: CardsService,
              private transaction: TransactionsService,
              private route: Router,
              private auth: AuthService) { }

  ngOnInit() {
    this.getCards();
    this.getToken();
  }

  private getToken(){
    this.auth.getToken().subscribe((data) => {
      let infoToken = Object.values(data);
      this.secretKey = infoToken[0].toString();
    })
  }

  public getCards(){
    const subscripction = this.serviceCards.getCards().subscribe((data)=> {
      this.cards = Object.values(data)
      this.numberProduct = this.cards[0].productNumber;
    }, err => {
      console.error(err);
    });
    subscripction.add(() => {
      this.getTransactionForProduct(this.numberProduct)
    })
  }

  getTransactionForProduct(numberProduct: string){
    this.transaction.getTransactionsForProduct(numberProduct).subscribe((data)=> {
      this.infoTransactions = Object.values(data);
    })
  }

  choiseCard(card: ICards){
    let { productNumber } = card;
    this.getTransactionForProduct(productNumber);
    this.encryptedText = CryptoJS.AES.encrypt(productNumber.trim(),this.secretKey.trim()).toString();
    this.route.navigate(['/detail-product', this.encryptedText]);
    this.InfoCard.emit(card);
  }

}
